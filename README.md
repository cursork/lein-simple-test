# lein-simple-test

A Leiningen plugin to run simple-test tests. Amazing.

## License

Copyright © 2014 Neil Kirsopp

Distributed under the MIT License.
