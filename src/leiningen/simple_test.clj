(ns leiningen.simple-test
  (:require [clojure.pprint :refer (pprint)]
            [leiningen.core.eval :refer (eval-in-project)]
            [leiningen.core.main :refer (exit)]))

(defn once
  [project]
  ;; TODO only supports one dir right now! Need to summarise the summarised :/
  ;; TODO need to add some formatting to simple-test
  ;; TODO figure out how to get an exit code out to lein
  (let [[tdir] (:test-paths project)]
    (eval-in-project
      project
      `(if-let [results# (simple-test/test-directory ~tdir)]
         ;; TODO System/exit is a TEMPORARY HACK
         (do (pprint results#)
             (System/exit (int (:total-fails results#))))
         (do (println "Couldn't run simple-test tests!")
             (System/exit (int 255))))
      '(require 'simple-test))))

(defn simple-test
  "Run simple-test tests"
  [project & [cmd & args]]
  (case cmd
    "once" (once project)
    (println "Available commands: once\n")))

