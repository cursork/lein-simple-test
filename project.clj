(defproject lein-simple-test "0.1.0-SNAPSHOT"
  :description "Super Simple Clojure Testing In A Leiningen Plugin"
  :url "http://bitbucket.org/cursork/lein-simple-test"
  :license {:name "MIT"
            :url "http://opensource.org/licenses/MIT"}
  :dependencies []
  :eval-in-leiningen true)
